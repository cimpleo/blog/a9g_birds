#include <api_os.h>
#include <api_info.h>    //IMEI of GSM module
#include <api_event.h>   //event
#include <api_network.h> //network
#include "assert.h"
#include "api_socket.h"

#include <api_debug.h>   // Trace
#include <api_ss.h>      //Send USSD
#include <api_charset.h> //Unicode2LocalLanguage

#include <stdbool.h> //bool
#include "api_sms.h" // SMS library
#include <time.h>

#include "stdint.h" // uint8_t
#include "stdio.h"  // snprint
#include <string.h> // String

#include "api_fs.h"
#include <api_gps.h>
#include <api_hal_uart.h>
#include "buffer.h"
#include "gps_parse.h"
#include "math.h"
#include "gps.h"
#include "api_hal_pm.h"
#include "api_hal_gpio.h"
#include "api_hal_watchdog.h"

#include "adc_task.h"
#include "gps_search.h"
#include "gps_task.h"
#include "main_task.h"
#include "mqtt_task.h"
#include "pm_task.h"
#include "send_task.h"

HANDLE sendTaskHandle = NULL;
HANDLE semUSSDSend = NULL;
HANDLE semSendStart = NULL;
HANDLE semAttachActivatePost = NULL;

char SendingBuffer[1024] = "";
char SendingBufferRAM[1024] = "";

void RAMdataWrite(char *BufferRAM)
{
    sprintf(SendingBufferRAM, BufferRAM);
    Trace(1, "SendingBufferRAM = %s", SendingBufferRAM);
}

int Http_Post(const char *domain, int port, const char *path, uint8_t *body, uint16_t bodyLen, char *retBuffer, int bufferLen)
{
    uint8_t ip[16];
    bool flag = false;
    uint16_t recvLen = 0;

    //connect server
    memset(ip, 0, sizeof(ip));
    if (DNS_GetHostByName2(domain, ip) != 0)
    {
        Trace(2, "get ip error");
        return -1;
    }
    // Trace(2,"get ip success:%s -> %s",domain,ip);
    char *servInetAddr = ip;
    char *temp = OS_Malloc(2048);
    if (!temp)
    {
        Trace(2, "malloc fail");
        return -1;
    }
    snprintf(temp, 2048, "POST %s HTTP/1.1\r\nContent-Type: application/json\r\nContent-Length: %d\r\nHost: %s\r\n\r\n%s",
             path, bodyLen, domain, body);
    char *pData = temp;
    int fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (fd < 0)
    {
        Trace(2, "socket fail");
        OS_Free(temp);
        return -1;
    }
    // Trace(2,"fd:%d",fd);

    struct sockaddr_in sockaddr;
    memset(&sockaddr, 0, sizeof(sockaddr));
    sockaddr.sin_family = AF_INET;
    sockaddr.sin_port = htons(port);
    inet_pton(AF_INET, servInetAddr, &sockaddr.sin_addr);

    int ret = connect(fd, (struct sockaddr *)&sockaddr, sizeof(struct sockaddr_in));
    if (ret < 0)
    {
        Trace(2, "socket connect fail");
        OS_Free(temp);
        close(fd);
        return -1;
    }
    // Trace(2,"socket connect success");
    Trace(2, "send request:%s", pData);
    ret = send(fd, pData, strlen(pData), 0);
    if (ret < 0)
    {
        Trace(2, "socket send fail");
        OS_Free(temp);
        close(fd);
        return -1;
    }
    ret = send(fd, body, bodyLen, 0);
    if (ret < 0)
    {
        Trace(2, "socket send fail");
        OS_Free(temp);
        close(fd);
        return -1;
    }
    // Trace(2,"socket send success");

    struct fd_set fds;
    struct timeval timeout = {12, 0};
    FD_ZERO(&fds);
    FD_SET(fd, &fds);
    while (!flag)
    {
        ret = select(fd + 1, &fds, NULL, NULL, &timeout);
        // Trace(2,"select return:%d",ret);
        switch (ret)
        {
        case -1:
            Trace(2, "select error");
            flag = true;
            break;
        case 0:
            Trace(2, "select timeout");
            flag = true;
            break;
        default:
            if (FD_ISSET(fd, &fds))
            {
                memset(retBuffer, 0, bufferLen);
                ret = recv(fd, retBuffer, bufferLen, 0);
                recvLen += ret;
                if (ret < 0)
                {
                    Trace(2, "recv error");
                    flag = true;
                    break;
                }
                else if (ret == 0)
                {
                    Trace(2, "ret == 0");
                    break;
                }
                else if (ret < 1352)
                {
                    GPS_DEBUG_I("recv len:%d,data:%s", recvLen, retBuffer);
                    close(fd);
                    OS_Free(temp);
                    return recvLen;
                }
            }
            break;
        }
    }
    close(fd);
    OS_Free(temp);
    return -1;
}

void Send_data_post()
{
    uint8_t buffer_post[1024], buffer2_post[400], buffer3_post[400];
    char *requestPath = buffer3_post;
    char *bodyPath = buffer3_post;

    snprintf(requestPath, sizeof(buffer2_post), "/api/store");

    snprintf(bodyPath, sizeof(buffer2_post), "{\"data\":\"%s\"}",
             SendingBuffer);
    Trace(1, "requestPath = %s", requestPath);

    AttachActivateInit();

    semAttachActivatePost = OS_CreateSemaphore(0);
    OS_WaitForSemaphore(semAttachActivatePost, OS_WAIT_FOREVER);
    OS_DeleteSemaphore(semAttachActivatePost);
    semAttachActivatePost = NULL;
    uint8_t status;
    Network_GetActiveStatus(&status);
    if (status)
    {

        if (Http_Post(SERVER_IP, SERVER_PORT, requestPath, bodyPath, strlen(bodyPath), buffer_post, sizeof(buffer_post)) < 0)
        {

            Trace(1, "send location to server fail");
        }
        else
        {
            Trace(1, "===send location to server success====");
            Trace(1, "strlen %d", strlen(bodyPath));
            Trace(1, "bodyPath = %s", bodyPath);
            Trace(1, "response:%s", buffer_post);
            OS_ReleaseSemaphore(semPmStart); // into PM_task ---------------------------------------->
        }
    }
    else
    {
        Trace(1, "no internet");
    }
}

void SMSInit()
{
    if (!SMS_SetFormat(SMS_FORMAT_TEXT, SIM0))
    {
        Trace(1, "sms set format error");
        return;
    }
    SMS_Parameter_t smsParam = {
        .fo = 17,
        .vp = 167,
        .pid = 0,
        .dcs = 8, //0:English 7bit, 4:English 8 bit, 8:Unicode 2 Bytes
    };
    if (!SMS_SetParameter(&smsParam, SIM0))
    {
        Trace(1, "sms set parameter error");
        return;
    }
    if (!SMS_SetNewMessageStorage(SMS_STORAGE_SIM_CARD))
    {
        Trace(1, "sms set message storage fail");
        return;
    }
}

void Send_data_sms()
{
    uint8_t *unicode = NULL;
    uint32_t unicodeLen;

    Trace(1, "sms start send UTF-8 message");
    if (!SMS_LocalLanguage2Unicode(SendingBuffer, strlen(SendingBuffer), CHARSET_UTF_8, &unicode, &unicodeLen))
    {
        Trace(1, "local to unicode fail!");
        return;
    }
    if (!SMS_SendMessage(PHONE_NUMBER_FOR_SMS, unicode, unicodeLen, SIM0))
    {
        Trace(1, "sms send message fail");
    }
    OS_Free(unicode);
}

void Send_data_ussd()
{
    char ussd[1024] = "";
    char code_ussd[] = "*100*";
    char code_ussd_end[] = "#";
    Trace(1, "SendingBuffer = %s", SendingBuffer);
    sprintf(ussd, "%s%s%s", code_ussd, SendingBuffer, code_ussd_end);
    Trace(1, "USSD sended = %s", ussd);
    uint8_t buffer[100];
    USSD_Type_t usd;

    int encodeLen = GSM_8BitTo7Bit(ussd, buffer, strlen(ussd));
    usd.usdString = buffer;
    usd.usdStringSize = encodeLen;
    usd.option = 3;
    usd.dcs = 0x0f;
    uint32_t ret2 = SS_SendUSSD(usd);
    Trace(1, "ussd ret:0x%x", ret2);
    WatchDog_KeepAlive();
}

void SendingAnyCh()
{
    Trace(1, "SendingAnyCh - start");
    WatchDog_KeepAlive();
#ifdef USSD
    if (OS_DeleteSemaphore(semUSSDSend) == true)
    {
        Send_data_ussd(); // function of sending data via USSD.
    }
    else
    {
        Trace(1, "Network fail");
        OS_ReleaseSemaphore(semPmStart);
    }
#endif

#ifdef SMS
    SMSInit();       //  SMS init func
    Send_data_sms(); // SMS func sending init
#endif
#ifdef POST
    AttachActivateInit();
    Send_data_post();
#endif
#ifdef MQTT
    AttachActivateInit();
    OS_ReleaseSemaphore(semMqttStart);
#endif
    OS_Sleep(DELAY_SENDING); // pause between dispatches
    Trace(1, "Sending Buffer_Free");
    strcpy(SendingBuffer, "");    // clear buffer
    strcpy(SendingBufferRAM, ""); // clear buffer
}

bool FsTFread()
{
    int32_t fd;
    int32_t ret;
    uint8_t *path = TF_CARD_TEST_FILE_NAME;
    char bufferString[1024];
    char bufferTF2[1024];    
    Trace(1, "==== Start Fs  TF card READ! ====");
    fd = API_FS_Open(path, FS_O_RDONLY, 0); 
    if (fd < 0)
    {
        Trace(1, "Open file failed:%d", fd);
        return false; 
    }
    ret = API_FS_Read(fd, bufferString, 1024); 
    if (ret <= 0)
    {
        Trace(1, "read fail");
        return false; 
    }
    Trace(1, "Symbol count (ret):%d", ret); 
    Trace(1, "Coordinates count:%d", ret/52); 
    Trace(1, "Symbol count (ret END):%d", ret); 
    Trace(1, "Buffer:%s", bufferString); 
    if (ret > 1023)
        ret = 1024;
    strcpy(bufferTF2, "");
    strcpy(SendingBuffer, ""); 
    for (uint32_t i = 0; i < ret; i++)
    {
        if (bufferString[i] != '\n')
        {
            snprintf(bufferTF2, 1024, "%c", bufferString[i]);
            strcat(SendingBuffer, bufferTF2);
        }
        else
        {
            Trace(1, "Sending Buffer =%s", SendingBuffer);
            Trace(1, "i = %d", i); 
            Trace(1, "Count coordinates = %d / %d", i/52, ret/52);
            SendingAnyCh();
        }
    }

    Trace(1, "FS_CLOSE");
    API_FS_Close(fd);
    Trace(1, "Delete file %s", path);
    API_FS_Delete(path); // after sending, delete the file on the TF card.
    return true;
}

void SendTask(void *pData)
{
    semSendStart = OS_CreateSemaphore(0);
    OS_WaitForSemaphore(semSendStart, OS_WAIT_FOREVER);
    OS_DeleteSemaphore(semSendStart);
    semSendStart = NULL;
    Trace(1, "SEND Task Start");
    if (FsTFread() == true)
    {
        OS_ReleaseSemaphore(semPmStart);
    }
    else
    {
        Trace(1, "TF read false, sending location from RAM");
        strcpy(SendingBuffer, "");
        sprintf(SendingBuffer, SendingBufferRAM);
        SendingAnyCh();
        OS_ReleaseSemaphore(semPmStart);
    }
}

void SendTaskInit(void)
{
    sendTaskHandle = OS_CreateTask(SendTask,
                                   NULL, NULL, SEND_TASK_STACK_SIZE, SEND_TASK_PRIORITY, 0, 0, SEND_TASK_NAME);
}
