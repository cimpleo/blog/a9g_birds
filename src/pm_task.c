
#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"
#include "string.h"

#include "api_os.h"
#include "api_debug.h"
#include "api_event.h"
#include "api_hal_pm.h"
#include "api_key.h"
#include "time.h"
#include "pm_task.h"
#include "api_hal_watchdog.h"
#include "mqtt_task.h"
#include "main_task.h"
#include "gps_task.h"
#include "gps_search.h"
#include "gps.h"
#include "api_network.h"
#include "adc_task.h"
#include "send_task.h"
#include "api_hal_gpio.h"
#include <api_hal_adc.h>
#include <api_hal_i2c.h>
#include <api_hal_spi.h>
#include <api_hal_uart.h>

static HANDLE pmTaskHandle = NULL;
HANDLE semPmStart = NULL;

void OnTimer(void *param)
{
    semPmStart = OS_CreateSemaphore(0);
    OS_WaitForSemaphore(semPmStart, OS_WAIT_FOREVER);
    OS_DeleteSemaphore(semPmStart);
    semPmStart = NULL;
    

    Trace(1, "#1 Delete ADC task");
    OS_StopTask(adcTaskHandle);
    OS_DeleteTask(adcTaskHandle);

#ifdef MQTT
    Trace(1, "Delete MQTT task");
    OS_StopTask(mqttTaskHandle);
    OS_DeleteTask(mqttTaskHandle);

    Trace(1, "WatchDog Close");
    WatchDog_Close();
#endif

    Trace(1, "#2 Delete GPS task");
    OS_StopTask(gpsTaskHandle);
    OS_DeleteTask(gpsTaskHandle);

    Trace(1, "#3 Delete Send task");
    OS_StopTask(sendTaskHandle);
    OS_DeleteTask(sendTaskHandle);
    GPS_Close();

    Trace(1, "#4 Delete GPS-Search task");
    OS_StopTask(networkTaskHandle);
    OS_DeleteTask(networkTaskHandle);

    Trace(1, "#5 GPRS Close");
    Network_StartDetach();
    Network_StartDeactive(1);
    Network_DeRegister();

    Trace(1, "#6 GPIO Close");
    GPIO_Close(GPIO_PIN_MAX);
    Trace(1, "#7 ADC Close");
    ADC_Close(ADC_CHANNEL_MAX);

    Trace(1, "#8 UART Close");
    UART_Close(UART_PORT_MAX);

    Trace(1, "Performance PM_SYS_FREQ_32K");
    PM_SetSysMinFreq(PM_SYS_FREQ_32K);

    Trace(1, "WatchDog Close");
    WatchDog_Close();

   uint8_t tempcount=0; // uint8_t = 0 - 255  
PM_SleepMode(true);
Trace(1,"Enter deep sleep");
for (tempcount=0; tempcount < 60;tempcount++) //60 == 1 hour; 240 == 4 hour 
{
Trace(1,"Sleep %d minutes",tempcount);OS_Sleep(60 * 1000);//1 minute
}
PM_SleepMode(false);
Trace(1,"Exit deep sleep");
    
    Trace(1, "reboot system");
    PM_Reboot();
}

void PmTaskInit()
{
    pmTaskHandle = OS_CreateTask(OnTimer,
                                 NULL, NULL, PM_TASK_STACK_SIZE, PM_TASK_PRIORITY, 0, 0, PM_TASK_NAME);
}
