#include <string.h>
#include <stdio.h>

#include <api_os.h>
#include <api_info.h>
#include <api_event.h>
#include <api_network.h>
#include <api_debug.h>
#include <api_gps.h>
#include <api_ss.h>
#include <api_charset.h>
#include <stdbool.h>
#include <gps.h>
#include <gps_parse.h>
#include <api_hal_watchdog.h>
#include <time.h>
#include <api_hal_gpio.h>
#include "api_fs.h"

#include "adc_task.h"
#include "gps_search.h"
#include "gps_task.h"
#include "main_task.h"
#include "mqtt_task.h"
#include "pm_task.h"
#include "send_task.h"

HANDLE networkTaskHandle = NULL;
HANDLE semGPSsearch = NULL;

char buffer_temp[1024] = "";
char splitter[] = ";";
    
    GPIO_config_t stateLedred = {
    .mode = GPIO_MODE_OUTPUT,
    .pin = GPIO_PIN27,
    .defaultLevel = GPIO_LEVEL_HIGH,
};

GPIO_config_t stateLedredend = {
    .mode = GPIO_MODE_OUTPUT,
    .pin = GPIO_PIN27,
    .defaultLevel = GPIO_LEVEL_LOW,
};

//recording GPS data on the TF card
bool FsTFwrite(char *BufferPath)
{
    Trace(1, "==== Start Fs  TF card WRITE! ====");
    int32_t fd;
    int32_t ret;
    uint8_t *path = TF_CARD_TEST_FILE_NAME;

    fd = API_FS_Open(path, FS_O_RDWR | FS_O_APPEND | FS_O_CREAT, 0); // opening a file on a TF card.
    if (fd < 0)
    {
        Trace(1, "Open file failed:%d", fd);
        return false;
    }
    ret = API_FS_Write(fd, (uint8_t *)BufferPath, strlen(BufferPath)); // writing data to a file

    if (ret <= 0)
    {
        Trace(1, "write fail");
        return false;
    }
    Trace(1, "Write success");
    strcpy(buffer_temp, ""); // clear buffer
    Trace(1, "API_FS_Close");
    API_FS_Close(fd);
    return true;
}

void Test_MainTask(void *param)
{
    semGPSsearch = OS_CreateSemaphore(0);               // create semaphore
    OS_WaitForSemaphore(semGPSsearch, OS_WAIT_FOREVER); // waiting for a semaphore to run a function
    OS_DeleteSemaphore(semGPSsearch);                   // after starting, we remove the semaphore so that it does not wait any longer
    semGPSsearch = NULL;
    Trace(1, "=====GPS search start=====");
    bool search = true;
    uint8_t timer = 0;

    while (search)
    {
        GPS_Info_t *gpsInfo = Gps_GetInfo();
        //show fix info
        uint8_t isFixed = gpsInfo->gsa[0].fix_type > gpsInfo->gsa[1].fix_type ? gpsInfo->gsa[0].fix_type : gpsInfo->gsa[1].fix_type;
        //convert unit ddmm.mmmm to degree(°)
        int temp = (int)(gpsInfo->rmc.latitude.value / gpsInfo->rmc.latitude.scale / 100);
        double latitude = temp + (double)(gpsInfo->rmc.latitude.value - temp * gpsInfo->rmc.latitude.scale * 100) / gpsInfo->rmc.latitude.scale / 60.0;
        temp = (int)(gpsInfo->rmc.longitude.value / gpsInfo->rmc.longitude.scale / 100);
        double longitude = temp + (double)(gpsInfo->rmc.longitude.value - temp * gpsInfo->rmc.longitude.scale * 100) / gpsInfo->rmc.longitude.scale / 60.0;
        //char imei[16] = IMEI_ADDRESS;
        char imei[16] = "";
        INFO_GetIMEI(imei);

        if (isFixed == 2 || isFixed == 3 || gpsInfo->gga.satellites_tracked>2)
       // if (gpsInfo->gga.satellites_tracked == 0)
        {
            WatchDog_KeepAlive();
            char *isFixedStr;
            if (isFixed == 2)
            {
                isFixedStr = "2D fix";
                Trace(1, "2D fix");
            }
            else if (isFixed == 3)
            {
                if (gpsInfo->gga.fix_quality == 1)
                {
                    isFixedStr = "3D fix";
                    Trace(1, "3D fix");
                }
                else if (gpsInfo->gga.fix_quality == 2)
                {
                    isFixedStr = "3D/DGPS fix";
                    Trace(1, "3D/DGPS fix");
                }
            }
            else
                isFixedStr = "no fix";

            if (gpsInfo->gga.satellites_tracked > 2)
            {
                Trace(1, "===GPS FIXED - Sat.tracker =%d ===", gpsInfo->gga.satellites_tracked);
            }
            Trace(1, "===GPS Tracker - FIXED! ===");
            Trace(1, "Location: %f, %f", latitude, longitude);
            Trace(1, "IMEI: %s", imei);

            sprintf(buffer_temp, "%f%s%f%s%d%s%s%s%.02f\n", latitude, splitter, longitude, splitter, time(NULL), splitter, imei, splitter, GetLiionVoltage());
            search = false;
            for (int i = 0; i < 10; i++)
            {
                GPIO_Init(stateLedred);
                OS_Sleep(100);
                GPIO_Init(stateLedredend);
                OS_Sleep(100);
            }  
            RAMdataWrite(buffer_temp);
            FsTFwrite(buffer_temp);
            OS_ReleaseSemaphore(semSendStart);
        }
        else
        {
            GPIO_Init(stateLedred);
            OS_Sleep(100);
            GPIO_Init(stateLedredend);
            OS_Sleep(100);
            Trace(1, "no fix || satellites tracked:%d, gps sates total:%d, Latitude:%f, Longitude:%f, Attempt:%d/%d", gpsInfo->gga.satellites_tracked, gpsInfo->gsv[0].total_sats, latitude, longitude, timer, ATTEMPT);
            WatchDog_KeepAlive();
            timer++;
            OS_Sleep(5000);
        }
        if (timer > ATTEMPT)
        {
            search = false;
            Trace(1, "GPS Close");
            GPS_Close();
            OS_ReleaseSemaphore(semSendStart);
        }
    }
}

void GPS_SEARCH_TaskInit()
{
    networkTaskHandle = OS_CreateTask(Test_MainTask,
                                      NULL, NULL, GPS_SEARCH_TASK_STACK_SIZE, GPS_SEARCH_TASK_PRIORITY, 0, 0, GPS_SEARCH_TASK_NAME);
}
