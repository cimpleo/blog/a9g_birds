#include <string.h>
#include <stdio.h>
#include <api_os.h>
#include <api_call.h>
#include <api_gps.h>
#include <api_event.h>
#include <api_charset.h>
#include <api_hal_uart.h>
#include <api_debug.h>
#include "buffer.h"
#include "gps_parse.h"
#include "math.h"
#include "gps.h"
#include "api_mqtt.h"
#include "api_key.h"
#include "time.h"
#include "api_info.h"
#include "assert.h"
#include "api_socket.h"
#include "api_network.h"
#include "api_hal_gpio.h"
#include "api_hal_adc.h"
#include "api_hal_pm.h"
#include "api_hal_watchdog.h"
#include "adc_task.h"
#include "gps_search.h"
#include "gps_task.h"
#include "main_task.h"
#include "mqtt_task.h"
#include "pm_task.h"
#include "send_task.h"

HANDLE mainTaskHandle = NULL;

bool AttachActivate()
{
    uint8_t status;
    bool ret = Network_GetAttachStatus(&status);
    if (!ret)
    {
        Trace(2, "get attach staus fail");
        return false;
    }
    Trace(2, "attach status:%d", status);
    if (!status)
    {
        ret = Network_StartAttach();
        if (!ret)
        {
            Trace(2, "network attach fail");
            return false;
        }
    }
    else
    {
        ret = Network_GetActiveStatus(&status);
        if (!ret)
        {
            Trace(2, "get activate staus fail");
            return false;
        }
        Trace(2, "activate status:%d", status);
        if (!status)
        {
            Network_PDP_Context_t context = {
                .apn = PDP_CONTEXT_APN,
                .userName = PDP_CONTEXT_USERNAME,
                .userPasswd = PDP_CONTEXT_PASSWD};
            Network_StartActive(context);
        }
    }
    return true;
}
void AttachActivateInit()
{
    Trace(1, "NETWORK activate init");
    AttachActivate();
}

void EventDispatch(API_Event_t *pEvent)
{
    switch (pEvent->id)
    {
    case API_EVENT_ID_SIMCARD_DROP:
        Trace(2, "SIMCARD_DROP");
        break;

    case API_EVENT_ID_NO_SIMCARD:
        Trace(2, "!!!!NO SIM CARD%d!!!!", pEvent->param1);
        break;

    case API_EVENT_ID_NETWORK_REGISTER_SEARCHING:
        Trace(1, "REGISTER SEARCHING");
        break;

    case API_EVENT_ID_NETWORK_DEREGISTER:
        Trace(1, "REGISTER DEREGISTER");
        break;

    case API_EVENT_ID_NETWORK_REGISTER_DENIED:
        Trace(2, "network register denied");

    case API_EVENT_ID_NETWORK_REGISTER_NO:
        Trace(2, "network register no");
        break;

    case API_EVENT_ID_NETWORK_REGISTERED_HOME:
        Trace(2, "SIM network register HOME success");
        semUSSDSend = OS_CreateSemaphore(0);
        OS_ReleaseSemaphore(semUSSDSend);
        break;

    case API_EVENT_ID_NETWORK_REGISTERED_ROAMING:
        Trace(2, "SIM network register ROAMING success");
        semUSSDSend = OS_CreateSemaphore(0);
        OS_ReleaseSemaphore(semUSSDSend);
        break;

    case API_EVENT_ID_NETWORK_GOT_TIME:
        Trace(2, "network got time");
        break;

    case API_EVENT_ID_NETWORK_DETACHED:
        Trace(2, "network detached");
        break;
    case API_EVENT_ID_NETWORK_ATTACH_FAILED:
        Trace(2, "network attach failed");
        OS_ReleaseSemaphore(semPmStart); // into PM_task ---------------------------------------->
        break;

    case API_EVENT_ID_NETWORK_ATTACHED:
        Trace(2, "network attach success");
        break;

    case API_EVENT_ID_NETWORK_DEACTIVED:
        Trace(2, "network deactived");
        break;

    case API_EVENT_ID_NETWORK_ACTIVATE_FAILED:
        Trace(2, "network activate failed");
        OS_ReleaseSemaphore(semPmStart); // into PM_task ---------------------------------------->
        break;

    case API_EVENT_ID_NETWORK_ACTIVATED:
        Trace(2, "network activate success");
#ifdef MQTT
        OS_ReleaseSemaphore(semAttachActivateStart);
#endif
#ifdef POST
        OS_ReleaseSemaphore(semAttachActivatePost);
#endif
        break;

    case API_EVENT_ID_GPS_UART_RECEIVED:
        // Trace(1, "received GPS data, length:%d, data:%s, flag:%d", pEvent->param1, pEvent->pParam1, flag);
        GPS_Update(pEvent->pParam1, pEvent->param1);
        break;

    case API_EVENT_ID_SIGNAL_QUALITY:
        Trace(2, "CSQ:%d", pEvent->param1);
        break;

        break;

#ifdef USSD
    case API_EVENT_ID_USSD_SEND_SUCCESS:
    {
        uint8_t buffer[100];
        Trace(1, "ussd execute success");
        USSD_Type_t *result = (USSD_Type_t *)pEvent->pParam1;
        int len = GSM_8BitTo7Bit(result->usdString, buffer, result->usdStringSize);
        Trace(1, "string:%s,size:%d,option:%d,dcs:%d", buffer, len, result->dcs, result->option);
        //string:×ô4Ïª É,size:8,option:15,dcs:2
        break;
    }
    case API_EVENT_ID_USSD_SEND_FAIL:
        Trace(1, "ussd exec fail,error code:%x,%d", pEvent->param1, pEvent->param2);
        break;

    case API_EVENT_ID_USSD_IND:
    {
        uint8_t buffer[100];
        Trace(1, "ussd recieved");
        USSD_Type_t *result = (USSD_Type_t *)pEvent->pParam1;
        int len = GSM_7BitTo8Bit(result->usdString, buffer, result->usdStringSize);
        Trace(1, "string:%s,size:%d,option:%d,dcs:%d", buffer, len, result->dcs, result->option);
        break;
    }
#endif

#ifdef SMS
    case API_EVENT_ID_SMS_SENT:
        Trace(2, "Send Message Success");
        break;

    case API_EVENT_ID_SMS_ERROR:
        Trace(10, "SMS error occured! cause:%d", pEvent->param1);
        break;
#endif

    default:
        break;
    }
}

/**
 * The Main task
 *
 * Init HW and run MQTT task. 
 *
 * @param  pData Parameter passed in when this function is called
 */
void app_MainTask(void *pData)
{
    API_Event_t *event = NULL;

    Trace(1, "Main Task started");
    Trace(1, "WatchDog Open");
    WatchDog_Open(WATCHDOG_SECOND_TO_TICK(30));
    WatchDog_KeepAlive();
    TIME_SetIsAutoUpdateRtcTime(true); // Local time will be synced with GPS

    // Create ADC task
    AdcTaskInit();

#ifdef MQTT 
    // Create MQTT task
    MqttTaskInit();
#endif
    
    // Create GPS task
    GpsTaskInit();
    
    // Create GPS search task
    GPS_SEARCH_TaskInit();

    // Create Send task
    SendTaskInit();

    // Create power manager task
    PmTaskInit();

    // Wait and process system events
    while (1)
    {
        if (OS_WaitEvent(mainTaskHandle, (void **)&event, OS_TIME_OUT_WAIT_FOREVER))
        {
            EventDispatch(event);
            OS_Free(event->pParam1);
            OS_Free(event->pParam2);
            OS_Free(event);
        }
    }
}

void app_Main(void)
{
    mainTaskHandle = OS_CreateTask(app_MainTask,
                                   NULL, NULL, MAIN_TASK_STACK_SIZE, MAIN_TASK_PRIORITY, 0, 0, MAIN_TASK_NAME);
    OS_SetUserMainHandle(&mainTaskHandle);
}
