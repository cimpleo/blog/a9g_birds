#include "stdbool.h"
#include "stdint.h"
#include "api_event.h"
#include "api_network.h"
#include "api_hal_pm.h"
#include <string.h>
#include <stdio.h>

#include <api_os.h>
#include <api_info.h>
#include <api_gps.h>
#include <api_debug.h>
#include <api_mqtt.h>
#include <api_socket.h>

#include <api_hal_gpio.h>

#include <gps.h>
#include <gps_parse.h>

#include "mqtt_task.h"
#include "adc_task.h"
#include "gps_task.h"
#include "api_hal_watchdog.h"
#include "pm_task.h"
#include <time.h>
#include "api_fs.h"
#include "main_task.h"

HANDLE mqttTaskHandle = NULL;
HANDLE semMqttStart = NULL;
HANDLE semAttachActivateStart = NULL;
HANDLE semReadStart = NULL;
HANDLE semStop = NULL;

MQTT_Connect_Info_t ci;
MQTT_Client_t *mqttClient = NULL;

const char *ca_crt = CA_CRT;
const char *client_crt = CLIENT_CRT;
const char *client_key = CLIENT_KEY;

char imei[16] = "";
char mqttTrackerTopic[64] = "";
char mqttBuffer[1024] = "";

char Buffer[1024] = "";

MQTT_Status_t mqttStatus;

MQTT_Status_t getMqttState(void)
{
    return mqttStatus;
}

void MqttDisconnect(void)
{
    Trace(1, "MqttDisconnect()");
    OS_ReleaseSemaphore(semPmStart); // into PM_task ---------------------------------------->
    MQTT_Error_t err = MQTT_Disconnect(mqttClient);

    if (err != MQTT_ERROR_NONE)
    {
        Trace(1, "MQTT connect fail, error code: %d", err);
    }
}

void MqttInit(void)
{
    Trace(1, "MQTT init");

    INFO_GetIMEI(imei);
    Trace(1, "IMEI: %s", imei);

    snprintf(mqttTrackerTopic, sizeof(mqttTrackerTopic), MQTT_TRACKER_TOPIC_FORMAT, imei);

    mqttClient = MQTT_ClientNew();
    memset(&ci, 0, sizeof(MQTT_Connect_Info_t));

    ci.client_id = imei;
    ci.client_user = CLIENT_USER;
    ci.client_pass = CLIENT_PASS;
    ci.keep_alive = 20;
    ci.clean_session = 1;
    ci.use_ssl = false;
#if 0 // TLS
    ci.use_ssl = true;
    ci.ssl_verify_mode = MQTT_SSL_VERIFY_MODE_OPTIONAL;
    ci.ca_cert = ca_crt;
    ci.ca_crl = NULL;
    ci.client_cert = client_crt;
    ci.client_key  = client_key;
    ci.client_key_passwd = NULL;
    ci.broker_hostname = BROKER_HOSTNAME;
    ci.ssl_min_version = MQTT_SSL_VERSION_TLSv1_1;
    ci.ssl_max_version = MQTT_SSL_VERSION_TLSv1_2;
    ci.entropy_custom = "GPRS_A9";
#endif
}

void OnPublish(void* arg, MQTT_Error_t err)
{
    if(err == MQTT_ERROR_NONE)
        Trace(1,"MQTT publish success");
    else
        Trace(1,"MQTT publish error, error code:%d",err);
}

void SendLocation(const char *path)
{
    Trace(1, "Send_Location start");
    MQTT_Error_t err = MQTT_Publish(mqttClient, mqttTrackerTopic, path, 1024, 1, 1, 1, OnPublish, NULL);
    if (err != MQTT_ERROR_NONE)
        Trace(1, "MQTT publish tracker error, error code: %d", err);

        Trace(1, "Send_Location end");
}

bool FsTFreadd()
{
    int32_t fd;
    int32_t ret;
    uint8_t *path = TF_CARD_TEST_FILE_NAME;
    char bufferString[1024]; 
    char bufferTF2[1024];    
    Trace(1, "==== Start Fs  TF card READ! ====");

    fd = API_FS_Open(path, FS_O_RDONLY, 0); 
    if (fd < 0)
    {
        Trace(1, "Open file failed:%d", fd);
        return false;
    }

    ret = API_FS_Read(fd, bufferString, 1024); 
    if (ret <= 0)
    {
        Trace(1, "read fail");
        return false;
    }
    Trace(1, "Symbol count (ret):%d", ret); 
    Trace(1, "Buffer:%s", bufferString);  

    strcpy(bufferTF2, "");
    strcpy(Buffer, ""); 

    for (uint8_t i = 0; i < ret; i++)
    {
        if (bufferString[i] != '\n')
        {
            snprintf(bufferTF2, 1024, "%c", bufferString[i]); // write character by character to buffer up to "\n".
            strcat(Buffer, bufferTF2);
        }
        else
        {
            Trace(1, "Buffer_for sending =%s", Buffer);
            Trace(1, "i = %d", i); 
            if (mqttStatus == MQTT_STATUS_CONNECTED){
            SendLocation(Buffer);          // function for sending data
            }
            else
            {     Trace(1, "MQTT_STATUS_DISCONNECTED");
                 Trace(1, "FS_CLOSE");
                 API_FS_Close(fd);
                 return false;
            }
            
            OS_Sleep(2000); // pause between sending data.
            Trace(1, "Buffer_Free");
            strcpy(Buffer, ""); // clear buffer
        }
    }
    Trace(1, "FS_CLOSE");
    API_FS_Close(fd);
    return true;
}

void OnMqttConnect(MQTT_Client_t *client, void *arg, MQTT_Connection_Status_t status)
{
    Trace(1, "MQTT connection status: %d", status);

    if (status == MQTT_CONNECTION_ACCEPTED)
    {
        Trace(1, "MQTT succeed connect to broker");
        mqttStatus = MQTT_STATUS_CONNECTED;
        WatchDog_KeepAlive();
        FsTFreadd();
    }
    else
    {
        mqttStatus = MQTT_STATUS_DISCONNECTED;
    }
}

void MqttConnect(void)
{
    Trace(1, "MqttConnect()");

    MQTT_Error_t err = MQTT_Connect(mqttClient, BROKER_IP, BROKER_PORT, OnMqttConnect, NULL, &ci);

    if (err != MQTT_ERROR_NONE)
    {
        Trace(1, "MQTT connect fail, error code: %d", err);
    }

    mqttStatus = MQTT_STATUS_CONNECTING;
}

void MqttTask(void *pData)
{
    semMqttStart = OS_CreateSemaphore(0);
    OS_WaitForSemaphore(semMqttStart, OS_WAIT_FOREVER);
    OS_DeleteSemaphore(semMqttStart);
    semMqttStart = NULL;

     WatchDog_KeepAlive();

    semAttachActivateStart = OS_CreateSemaphore(0);
    OS_WaitForSemaphore(semAttachActivateStart, OS_WAIT_FOREVER);
    OS_DeleteSemaphore(semAttachActivateStart);
    semAttachActivateStart = NULL;

    WatchDog_KeepAlive();

    MqttInit();

    while (1)
    {
        switch (mqttStatus)
        {
        case MQTT_STATUS_DISCONNECTED:
            MqttConnect();
            break;

        case MQTT_STATUS_CONNECTING:
            Trace(1, "MQTT is connecting...");
            break;

        case MQTT_STATUS_CONNECTED:
            break;

        default:
            break;
        }

        OS_Sleep(1000);
    }
}

void MqttTaskInit(void)
{
    mqttTaskHandle = OS_CreateTask(MqttTask,
                                   NULL, NULL, MQTT_TASK_STACK_SIZE, MQTT_TASK_PRIORITY, 0, 0, MQTT_TASK_NAME);
}