#ifndef __MAIN_TASK_H_
#define __MAIN_TASK_H_

#define MAIN_TASK_STACK_SIZE    (2048 * 4)
#define MAIN_TASK_PRIORITY      0
#define MAIN_TASK_NAME          "Main Task"

#define PHONE_NUMBER_FOR_SMS "your_phone"

#define SERVER_IP "your_ip_server"
#define SERVER_PORT 8080

#define PDP_CONTEXT_APN "your_apn_sim"
#define PDP_CONTEXT_USERNAME ""
#define PDP_CONTEXT_PASSWD ""

#define ATTEMPT 12 // Number of attempts to search for GPS

//method of sending data
#define USSD //SMS, MQTT, USSD, POST  

#define TF_CARD_TEST_FILE_NAME "/t/GAT_location.txt" // location and name of the file on the TF card
#define DELAY_SENDING 7000 // Time between dispatches

extern HANDLE mainTaskHandle;
 
#endif /* __MAIN_TASK_H_ */


