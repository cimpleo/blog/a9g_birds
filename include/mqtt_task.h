#ifndef __MQTT_TASK_H_
#define __MQTT_TASK_H_

#define MQTT_TASK_STACK_SIZE       (2048 * 4)
#define MQTT_TASK_PRIORITY         1
#define MQTT_TASK_NAME             "MQTT Task"

#define MQTT_TRACKER_TOPIC_FORMAT  "vehicle/%s/tracker"

#define BROKER_IP  "your_broker_ip"
#define BROKER_PORT 1883
#define CLIENT_USER "your_name"
#define CLIENT_PASS "your_password"
#define SUBSCRIBE_TOPIC "your_topic_subscribe"
#define PUBLISH_TOPIC   "your_topic_publish"
#define PUBLISH_INTERVAL 10000 //10s
#define PUBLISH_PAYLOEAD "Hello! I'm A9G"

extern HANDLE semMqttStart;
extern HANDLE mqttTaskHandle;
extern HANDLE semAttachActivateStart;

#define MQTT_INTERVAL              1 * 60000

#define MQTT_WATCHDOG_INTERVAL     1 * 60

typedef enum{
    MQTT_EVENT_CONNECTED = 0,
    MQTT_EVENT_DISCONNECTED ,
    MQTT_EVENT_MAX
}MQTT_Event_ID_t;

typedef struct {
    MQTT_Event_ID_t id;
    MQTT_Client_t* client;
}MQTT_Event_t;

typedef enum{
    MQTT_STATUS_DISCONNECTED = 0,
    MQTT_STATUS_CONNECTING      ,
    MQTT_STATUS_CONNECTED       ,
    MQTT_STATUS_MAX
}MQTT_Status_t;

#define CA_CRT "";

#define CLIENT_CRT "";

#define CLIENT_KEY "";

void AttachActivateInit(void);
void MqttTaskInit(void);

#endif
