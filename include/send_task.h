#ifndef __SEND_H_
#define __SEND_H_

#define SEND_TASK_STACK_SIZE    (2048 * 4)
#define SEND_TASK_PRIORITY      1
#define SEND_TASK_NAME          "Send Task"

extern HANDLE sendTaskHandle;
extern HANDLE semSendStart;
extern HANDLE semUSSDSend;
extern HANDLE semAttachActivatePost;

void AttachActivateInit(void);
extern void SendTaskInit(void);
extern void RAMdataWrite(char *BufferRAM);

#endif /* __SEND_H_ */
