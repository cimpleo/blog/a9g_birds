#ifndef __PM_TASK_H_
#define __PM_TASK_H_

#define PM_TASK_STACK_SIZE (2048 * 2)
#define PM_TASK_PRIORITY 0
#define PM_TASK_NAME "Pm Task"

extern HANDLE semPmStart;

void PmTaskInit(void);

#endif /* __PM_TASK_H_ */