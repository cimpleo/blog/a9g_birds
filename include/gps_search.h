#ifndef __GPS_SEARCH_TASK_H_
#define __GPS_SEARCH_TASK_H_

#define GPS_SEARCH_TASK_STACK_SIZE    (2048 * 2)
#define GPS_SEARCH_TASK_PRIORITY      1
#define GPS_SEARCH_TASK_NAME          "GPS SEARCH Task"

extern HANDLE semGPSsearch;
extern HANDLE networkTaskHandle;

void GPS_SEARCH_TaskInit(void);

#endif /* __GPS_SEARCH_TASK_H_ */
